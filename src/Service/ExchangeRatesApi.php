<?php
namespace Opeepl\BackendTest\Service;

use Opeepl\BackendTest\Lib\Curl;

abstract class ExchangeRatesApi {

    protected $curl = null;

    public function __construct() {
        $this->curl = new Curl();
    }

}
