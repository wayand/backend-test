<?php
namespace Opeepl\BackendTest\Service;

use Opeepl\BackendTest\Service\ExchangeInterface;
use Opeepl\BackendTest\Service\ExchangeRatesApi;


class ForeignExchangeRatesApi extends ExchangeRatesApi implements ExchangeInterface {

    public function getAllCurrencies() {
        $this->curl->setUrl('https://api.exchangeratesapi.io/latest?base=USD');
        $result = json_decode($this->curl->getResponse(), true);
        $currencies = [];

        if (is_array($result['rates']) && !empty($result['rates'])) {
            foreach ($result['rates'] as $key => $val) {
                $currencies[] = $key;
            }
        }
        return $currencies;
    }

    public function exchangeCurrency(int $amount, string $fromCurrency, string $toCurrency) {
        $this->curl->setUrl('https://api.exchangeratesapi.io/latest?base=' . $fromCurrency . '&symbols=' . $toCurrency);
        $result = json_decode($this->curl->getResponse(), true);
        return round( $result['rates'][$toCurrency] * $amount, 2 );
    }
}