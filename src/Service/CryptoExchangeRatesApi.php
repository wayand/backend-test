<?php
namespace Opeepl\BackendTest\Service;

use Opeepl\BackendTest\Service\ExchangeInterface;
use Opeepl\BackendTest\Service\ExchangeRatesApi;
use Opeepl\BackendTest\Lib\Curl;


class CryptoExchangeRatesApi extends ExchangeRatesApi implements ExchangeInterface {

    public function getAllCurrencies() {
        return ['DKK', 'USD', 'CAD'];
    }

    public function exchangeCurrency(int $amount, string $fromCurrency, string $toCurrency) {
        return 1;
    }
}