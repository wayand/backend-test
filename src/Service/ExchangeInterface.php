<?php
namespace Opeepl\BackendTest\Service;

interface ExchangeInterface{

    public function getAllCurrencies();
    public function exchangeCurrency(int $amount, string $fromCurrency, string $toCurrency);
}