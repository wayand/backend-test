<?php
namespace Opeepl\BackendTest\Service;

use Opeepl\BackendTest\Service\ForeignExchangeRatesApi;
use Opeepl\BackendTest\Service\CryptoExchangeRatesApi;

/**
 * Main entrypoint for this library.
 */
class ExchangeRateService {

    private $supportedCurrencies = [];
    private $supportedCryptoCurrencies = [];

    private $exchangeApi = null;
    private $cryptoExchangeApi = null;

    public function __construct() {

        $this->exchangeApi = new ForeignExchangeRatesApi();
        $this->cryptoExchangeApi = new CryptoExchangeRatesApi();
    }

    /**
     * Return all supported currencies
     *
     * @return array<string>
     */
    public function getSupportedCurrencies(): array {
        if (empty($this->supportedCurrencies)) {
            $this->loadSupportedCurrencies();
        }
        return $this->supportedCurrencies;
    }

    private function loadSupportedCurrencies() {
        
        $this->supportedCurrencies = $this->exchangeApi->getAllCurrencies();
        return $this->supportedCurrencies;
    }


    /**
     * Return all supported crypto currencies
     *
     * @return array<string>
     */
    public function getSupportedCryptoCurrencies(): array {
        if (empty($this->supportedCryptoCurrencies)) {
            $this->loadSupportedCryptoCurrencies();
        }
        return $this->supportedCryptoCurrencies;
    }

    private function loadSupportedCryptoCurrencies() {
        
        $this->supportedCryptoCurrencies = $this->cryptoExchangeApi->getAllCurrencies();
        return $this->supportedCryptoCurrencies;
    }

    /**
     * Given the $amount in $fromCurrency, it returns the corresponding amount in $toCurrency.
     *
     * @param int $amount
     * @param string $fromCurrency
     * @param string $toCurrency
     * @return int
     */
    public function getExchangeAmount(int $amount, string $fromCurrency, string $toCurrency): int {
        return $this->exchangeApi->exchangeCurrency($amount, $fromCurrency, $toCurrency);
    }

    /**
     * 
     * @param int $amount
     * @param string $fromCurrency
     * @param string $toCurrency
     * @return int
     */
    public function getCryptoExchangeAmount(int $amount, string $fromCurrency, string $toCurrency): int {
        return $this->cryptoExchangeApi->exchangeCurrency($amount, $fromCurrency, $toCurrency);
    }
}
